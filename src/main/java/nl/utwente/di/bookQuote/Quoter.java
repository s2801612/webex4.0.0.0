package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private final HashMap<String, Double> isbnToPriceMap = new HashMap<String, Double>();
    public double getBookPrice(String isbn) {
        isbnToPriceMap.put("1", 10.0);
        isbnToPriceMap.put("2", 45.0);
        isbnToPriceMap.put("3", 20.0);
        isbnToPriceMap.put("4", 35.0);
        isbnToPriceMap.put("5", 50.0);
        isbnToPriceMap.put("others", 0.0);
        return isbnToPriceMap.getOrDefault(isbn, 0.0);
    }
}
